json.array!(@generics) do |generic|
  json.extract! generic, :id, :one, :two, :three, :four, :five, :six, :seven, :eight, :nine, :ten, :eleven, :user_id
  json.url generic_url(generic, format: :json)
end
