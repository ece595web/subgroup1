json.array!(@collegeaands) do |collegeaand|
  json.extract! collegeaand, :id, :one, :two, :three, :four, :five, :six, :seven, :eight, :nine, :ten, :eleven, :twelve, :user_id
  json.url collegeaand_url(collegeaand, format: :json)
end
