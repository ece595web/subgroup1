json.array!(@schooles) do |schoole|
  json.extract! schoole, :id, :one, :two, :three, :four, :five, :six, :seven, :eight, :nine, :ten, :user_id
  json.url schoole_url(schoole, format: :json)
end
