class SchoolesController < ApplicationController
  before_action :set_schoole, only: [:show, :edit, :update, :destroy]

  # GET /schooles
  # GET /schooles.json
  def index
    @schooles = Schoole.all
  end

  # GET /schooles/1
  # GET /schooles/1.json
  def show
  end

  # GET /schooles/new
  def new
    @schoole = Schoole.new
  end

  # GET /schooles/1/edit
  def edit
  end

  # POST /schooles
  # POST /schooles.json
  def create
    @schoole = Schoole.new(schoole_params)

    respond_to do |format|
      if @schoole.save
        format.html { redirect_to @schoole, notice: 'Schoole was successfully created.' }
        format.json { render :show, status: :created, location: @schoole }
      else
        format.html { render :new }
        format.json { render json: @schoole.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /schooles/1
  # PATCH/PUT /schooles/1.json
  def update
    respond_to do |format|
      if @schoole.update(schoole_params)
        format.html { redirect_to @schoole, notice: 'Schoole was successfully updated.' }
        format.json { render :show, status: :ok, location: @schoole }
      else
        format.html { render :edit }
        format.json { render json: @schoole.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /schooles/1
  # DELETE /schooles/1.json
  def destroy
    @schoole.destroy
    respond_to do |format|
      format.html { redirect_to schooles_url, notice: 'Schoole was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_schoole
      @schoole = Schoole.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def schoole_params
      params.require(:schoole).permit(:one, :two, :three, :four, :five, :six, :seven, :eight, :nine, :ten, :user_id)
    end
end
