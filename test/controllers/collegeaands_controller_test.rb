require 'test_helper'

class CollegeaandsControllerTest < ActionController::TestCase
  setup do
    @collegeaand = collegeaands(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:collegeaands)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create collegeaand" do
    assert_difference('Collegeaand.count') do
      post :create, collegeaand: { eight: @collegeaand.eight, eleven: @collegeaand.eleven, five: @collegeaand.five, four: @collegeaand.four, nine: @collegeaand.nine, one: @collegeaand.one, seven: @collegeaand.seven, six: @collegeaand.six, ten: @collegeaand.ten, three: @collegeaand.three, twelve: @collegeaand.twelve, two: @collegeaand.two }
    end

    assert_redirected_to collegeaand_path(assigns(:collegeaand))
  end

  test "should show collegeaand" do
    get :show, id: @collegeaand
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @collegeaand
    assert_response :success
  end

  test "should update collegeaand" do
    patch :update, id: @collegeaand, collegeaand: { eight: @collegeaand.eight, eleven: @collegeaand.eleven, five: @collegeaand.five, four: @collegeaand.four, nine: @collegeaand.nine, one: @collegeaand.one, seven: @collegeaand.seven, six: @collegeaand.six, ten: @collegeaand.ten, three: @collegeaand.three, twelve: @collegeaand.twelve, two: @collegeaand.two }
    assert_redirected_to collegeaand_path(assigns(:collegeaand))
  end

  test "should destroy collegeaand" do
    assert_difference('Collegeaand.count', -1) do
      delete :destroy, id: @collegeaand
    end

    assert_redirected_to collegeaands_path
  end
end
